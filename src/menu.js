function activar_menu(menuId) {
    const submenu = document.getElementById(menuId);
    const enlaces = submenu.getElementsByTagName("a");

    for (let i = 0; i < enlaces.length; i++) {
        const element = enlaces[i];
        const nodeHref = element.getAttribute("href");
        if (location.href.endsWith(nodeHref)) {
            const nodeClass = document.createAttribute("class");
            nodeClass.value = "active";
            element.parentNode.setAttributeNode(nodeClass);
        }
    }
}