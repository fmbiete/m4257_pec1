const posthtml = require('posthtml');
const include = require('posthtml-include');
 
// core version + navigation, pagination modules:
import Swiper, { Autoplay } from 'swiper';
// import Swiper and modules styles
import 'swiper/swiper-bundle.css'


const swiper = new Swiper('.swiper', {
    modules: [Autoplay],
    // Optional parameters
    loop: true,
    // Disable preloading of all images
    preloadImages: false,
    // Enable lazy loading
    lazy: true,
});

swiper.autoplay.start();

